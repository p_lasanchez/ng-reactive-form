export const countries = [
  'USA',
  'Germany',
  'Italy',
  'France'
]
export const requestTypes = [
  'Claim',
  'Feedback',
  'Help Request'
]

export class PersonalData {
  email = ''
  confirmEmail = ''
  mobile = ''
  country = ''
}

export class ContactRequest {
  personalData: PersonalData
  requestType: string = null
  text: string = ''
}

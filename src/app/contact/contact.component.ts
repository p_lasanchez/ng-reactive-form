import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { countries, requestTypes, PersonalData, ContactRequest } from './model'
 
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {

  countries = countries
  requestTypes = requestTypes
  contactForm: FormGroup
  
  constructor(
    private formBuilder: FormBuilder
  ) {
    this.contactForm = this.createFormGroupWithBuilder(formBuilder)
  }

  equalitywith = (key1, key2): ValidatorFn => {
    return (control: AbstractControl) => {
      if (control.get(key2).value !== control.get(key1).value) {
        control.get(key2).setErrors({ NoPassswordMatch: true })
      }
      return null
    }
  }

  createFormGroupWithBuilder = (formBuilder: FormBuilder) => (
    formBuilder.group({
      personalData: formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        confirmEmail: ['', [Validators.required, Validators.email]],
        mobile: ['', Validators.required],
        country: ['', Validators.required]
      }),
      requestType: ['', Validators.required],
      text: ['', Validators.minLength(8)]
    },
    {
      validator: this.equalitywith('personalData.email', 'personalData.confirmEmail')
    })
  )

  handleSubmit = () => {
    const result: ContactRequest = Object.assign({}, this.contactForm.value)
    result.personalData = Object.assign({}, this.contactForm.value.personalData)
    console.log(result)
  }

  revert = () => {
    this.contactForm.reset({
      personalData: new PersonalData(),
      requestType: '',
      text: ''
    })
  }
}
